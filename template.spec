Name:       @NAME@

BuildArch: noarch

Summary:    @SUMMARY@
Version:    @VER@
Release:    1
Group:      Qt/Qt
License:    @LICENSE@
Source0:    %{name}-%{version}.tar.bz2
Requires:   patchmanager
#\\if @REBUILD@ == 0
BuildRequires: shpp
#\\endif
#\\ifdef SFOS_VERSION
Requires: sailfish-version == @SFOS_VERSION@
#\\endif

%description
@DESCRIPTION@

%prep
%setup -q -n %{name}-%{version}

%build
#\\if @REBUILD@ == 0
make
#\\endif
%install
rm -rf %{buildroot}
#\\if @REBUILD@ == 1
mkdir -p %{buildroot}/usr/share/patchmanager/patches/%Name
cp -r patch/* %{buildroot}/usr/share/patchmanager/patches/%Name
#\\else
make install DESTDIR=%{buildroot}
#\\endif

%pre
if [ -d /var/lib/patchmanager/ausmt/patches/%{name} ]; then
/usr/sbin/patchmanager -u %{name} || true
fi

%preun
if [ -d /var/lib/patchmanager/ausmt/patches/%{name} ]; then
/usr/sbin/patchmanager -u %{name} || true
fi

%files
%defattr(-,root,root,-)
%{_datadir}/patchmanager/patches/%{name}
