DIST_ROOT       = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
INSTALL    	= /usr/bin/install -D
DIFF 		= /usr/bin/diff -ru --new-file
SHPP		= /usr/bin/shpp
INSTALL_FILES   = $(DIST_ROOT)/scripts/install_files

TARGET		= unified_diff.patch
orig  		= orig
patched 	= patched
diff_root 	= to_diff
dir_install	= dir.install

DESTDIR 	?=
PREFIX		= $(DESTDIR)/usr
datarootdir	= $(PREFIX)/share
SHPPFLAGS 	?=

# selects wherever the patch is generated during pkg build
# or before
PREBUILD	= 0
# extends install
extra_install ?=


include 	project.mk

install_name 	= sailfishos-patch-$(shell echo $(NAME) | tr '[:upper:]' '[:lower:]'| \
			sed -e 's/ /-/g' )
patchdir 	:= $(datarootdir)/patchmanager/patches

all: $(TARGET) patch.json

$(TARGET): $(orig) $(patched) $(dir_install)
	cd $(orig); \
	$(INSTALL_FILES) -r ../$(diff_root)/$(orig) ../dir.install
	cd $(patched); \
	 $(INSTALL_FILES) -r ../$(diff_root)/$(patched) ../dir.install
	cd $(diff_root) ; \
	$(DIFF) $(orig) $(patched) > ../$@|| exit 0

install: $(TARGET) patch.json
	$(INSTALL) -m644 $(TARGET) $(patchdir)/$(install_name)/$(TARGET)
	$(INSTALL) -m644 patch.json $(patchdir)/$(install_name)/patch.json

patch.spec:  $(DIST_ROOT)/scripts/template.spec
	$(SHPP) -DNAME=$(install_name) \
		-DVER=$(VER) \
		-DSUMMARY=$(SUMMARY) \
		-DDESCRIPTION=$(DESCRIPTION) \
		-DLICENSE=$(LICENSE) \
		-DPREBUILD=$(PREBUILD) \
		$(<) -o $(@)

patch.json: $(DIST_ROOT)/scripts/patch.json.template
	$(SHPP) -DNAME=$(NAME) \
		-DDESCRIPTION=$(DESCRIPTION) \
		-DSUMMARY=$(SUMMARY) \
		-DCATEGORY=$(CATEGORY) \
		-DMAINTAINER=$(MAINTAINER) \
		$(<) -o $(@)
clean:
	rm  -f $(TARGET)
	rm -rf $(diff_root)
	rm -rf patch.spec
	rm -rf patch.json
	rm -rf $(diff_root)


.PHONY: clean install
